variable "name" {}
variable "env" {
    default = "Test"
}
variable "acl" {
    default = "private"
}
variable "versioning" {
    default = false
}
variable "prevent-destroy"{
    default = false
}


