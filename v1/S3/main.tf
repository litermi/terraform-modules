resource "aws_s3_bucket" "s3-bucket" {
  bucket = var.name
  acl    = var.acl

  versioning {
    # enable with caution, makes deleting S3 buckets tricky
    enabled = var.versioning
  }

  lifecycle {
    prevent_destroy = false
  }



  tags = {
    name = var.name
    env = var.env
    CreatedBy = "Terraform"
  }
}
