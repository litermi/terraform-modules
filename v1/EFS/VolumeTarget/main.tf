resource "aws_efs_mount_target" "mountTarget" {
  file_system_id = var.fileSystemId
  subnet_id      = var.subnetId
  security_groups = var.secGroupId
}
