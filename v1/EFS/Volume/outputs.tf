output "efsDns" {
    value = aws_efs_file_system.efsVolume.dns_name
}

output "efsId" {
    value = aws_efs_file_system.efsVolume.id
}
