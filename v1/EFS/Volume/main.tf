resource "aws_efs_file_system" "efsVolume" {
  creation_token = var.name

  lifecycle_policy {
    transition_to_ia = var.transitionToIA
  }

  tags = {
    Name = var.name 
    Env = var.env 
    CreatedBy = "Terraform"
  }
}
