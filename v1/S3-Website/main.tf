resource "aws_s3_bucket" "s3-bucket" {
  bucket = var.name
  region = var.region
  acl    = var.acl


  versioning {
    # enable with caution, makes deleting S3 buckets tricky
    enabled = var.versioning
  }

  lifecycle {
    prevent_destroy = true
  }
  
  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  tags = {
    name = var.name
    env = var.name
    CreatedBy = "Terraform"
  }
}
