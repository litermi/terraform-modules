output "bucket-id" {
  value = aws_s3_bucket.s3-bucket.id
}

output "endpoint" {
  value = aws_s3_bucket.s3-bucket.website_endpoint
}

