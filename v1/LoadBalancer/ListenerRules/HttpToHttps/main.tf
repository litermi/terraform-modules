resource "aws_lb_listener_rule" "HttpToHttps" {
  load_balancer_arn = var.loadBalancerArn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    target_group = var.targetGroup

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"

    }
  }
}
