
resource "aws_lb_listener" "listener" {
  load_balancer_arn = var.loadBalancerARN
  port              = var.port
  protocol          = var.protocol
  ssl_policy        = var.sslPolicy
  certificate_arn   = var.certARN

  default_action {
    type             = var.defaultActionType
    target_group_arn = var.targetGroupARN
  }
}