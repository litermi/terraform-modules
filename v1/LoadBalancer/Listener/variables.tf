variable "loadBalancerARN" {}
variable "port" {}
variable "protocol" {
    default = null
}
variable "sslPolicy" {
    default = null
}

variable "certARN" {
    default = null
}

variable "defaultActionType" {
    default = "forward"
}

variable "targetGroupARN" {}
