output "ALBARN" {
    value = aws_lb.alb.arn
}

output "DnsName"{
    value = aws_lb.alb.dns_name
}