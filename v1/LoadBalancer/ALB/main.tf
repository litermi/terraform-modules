resource "aws_lb" "alb" {
  name               = var.name
  internal           = var.internal
  load_balancer_type = "application"
  security_groups    = var.securityGroups
  subnets            = var.subnets
  enable_http2	     = false
  enable_deletion_protection = var.deleteProtection

  access_logs {
    bucket  = var.logsS3Bucket
    prefix  = var.logsPrefix
    enabled = var.logsEnabled
  }

  tags = {
    Environment = var.env
    Name = var.name
    CreatedBy = "Terraform"
  }
}
