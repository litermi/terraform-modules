variable "name" {}
variable "securityGroups" {
    type = list(string)
}
variable "subnets" {
    type = list(string)
}

variable "deleteProtection"{
    default = false
}

variable internal {
    default = false
}

variable "env" {
    default = "Test"
}

variable "logsEnabled"{
    default = false
}

variable "logsPrefix" {
    default = null
}
variable "logsS3Bucket" {
    default = "none"
}
