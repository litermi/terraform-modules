resource "aws_lb_target_group" "targetGroup" {
  name     = var.name
  port     = var.port
  protocol = var.protocol
  vpc_id   = var.vpc-id
  target_type = var.targetType
  stickiness {
      enabled = var.stickyness-enabled
      type = "lb_cookie"
      cookie_duration = var.stickyness-cookieDuration
  }
  health_check {
      enabled = var.healthcheck-enabled
      interval = var.healthcheck-interval
      path = var.healthcheck-path
      protocol = var.healthcheck-protocol
      timeout = var.healthcheck-timeout
      healthy_threshold = var.healthcheck-healthyThreshold
      unhealthy_threshold = var.healthcheck-unhealthyThreshold
      matcher = var.healthcheck-httpCode
  }
}
