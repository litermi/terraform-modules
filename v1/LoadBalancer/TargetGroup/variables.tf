variable "name" {}
variable "vpc-id" {}
variable "port" {
    default = 80
}

variable "protocol" {
    default = "HTTP"
}

variable "targetType" {
    default = "instance"
}

variable "stickyness-enabled"{
    default = true
}

variable "stickyness-cookieDuration"{
    default = 10800
}

variable "healthcheck-enabled" {
    default = true
}

variable "healthcheck-interval" {
    default = 120
}

variable "healthcheck-path" {
    default = "/"
}

variable "healthcheck-protocol" {
    default = "HTTP"
}

variable "healthcheck-timeout" {
    default = 60
}

variable "healthcheck-healthyThreshold" {
    default = 3
}

variable "healthcheck-unhealthyThreshold" {
    default = 3
}

variable "healthcheck-httpCode" {
    default = "200"
}
