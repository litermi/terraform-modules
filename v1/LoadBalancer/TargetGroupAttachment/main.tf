resource "aws_lb_target_group_attachment" "attachment" {
  target_group_arn = var.targetGroupARN
  target_id        = var.instanceID
  port             = var.port
}