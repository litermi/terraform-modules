variable "domain" {}
variable "alternativeNames"{
    default = null
    type = list(string)
}
variable "env" {
    default = "Test"
}