resource "aws_acm_certificate" "cert" {
  domain_name       = var.domain
  subject_alternative_names = var.alternativeNames

  validation_method = "DNS"

  tags = {
    Environment = var.env
    CreatedBy = "Terraform"
    Name = trim(var.domain, "*")
  }

  lifecycle {
    create_before_destroy = true
  }
}
