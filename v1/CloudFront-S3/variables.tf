variable "domainName" {}
variable "originId" {}
variable "originDomainName" {}
variable "aliases" {
    default = null
}

variable "priceClass" {
    default = "PriceClass_100"
}

variable "allowedMethods" {
    default = ["GET"]
    type = list(string)
}

variable "cachedMethods" {
    default = ["GET"]
    type = list(string)
}

variable "protocolPolicy" {
    default = "redirect-to-https"
}

variable "restrictionLocations" {
    default = null
}

variable "restrictionType" {
    default = "none"
}

variable "sslCert" {}
