

resource "aws_cloudfront_distribution" "site_distribution" {
  origin {
    domain_name = var.originDomainName
    origin_id = var.originId
  }
  enabled = true
  aliases = var.aliases
  price_class = var.priceClass
  default_root_object = "index.html"
  default_cache_behavior {
    allowed_methods  = var.allowedMethods
    cached_methods   = var.cachedMethods
    target_origin_id = var.originId
    forwarded_values {
      query_string = true
      cookies {
        forward = "all"
      }
    }
    viewer_protocol_policy = var.protocolPolicy
    min_ttl                = 0
    default_ttl            = 1000
    max_ttl                = 86400
  }
  restrictions {
    geo_restriction {
      locations = var.restrictionLocations
      restriction_type = var.restrictionType
    }
  }
  viewer_certificate {
    acm_certificate_arn = var.sslCert
    ssl_support_method  = "sni-only"
    minimum_protocol_version = "TLSv1.1_2016" # defaults wrong, set
  }
}
