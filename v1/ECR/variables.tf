variable "name" {}
variable "mutable" {
    default = "MUTABLE"
}
variable "scan-on-push" {
    default = true
}