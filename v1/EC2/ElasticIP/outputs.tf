# Define Public IP.
output "elastic-ip" {
  value = aws_eip.elastic-ip.public_ip
}
