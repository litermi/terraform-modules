# Add New Elastic IP To instance
resource "aws_eip" "elastic-ip" {
  instance = var.instance-id
  vpc      = true
  tags = {
    Name       = var.name
    CreatedBy  = "Terraform"

    Env        = var.env
  }
}



