variable "name" {}

variable "env" {
    default = "Test"
}

variable "availability-zone" {
    default = null
}

variable "size"{
    default = 12
}


variable "device-name" {}

variable "instance-id" {}
