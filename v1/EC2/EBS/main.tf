resource "aws_ebs_volume" "ebs-volume" {
  availability_zone = var.availability-zone
  size = var.size
  tags = {
    Name = var.name
    CreatedBy = "Terraform"
    Env = var.env
  }
}

resource "aws_volume_attachment" "ebs-volume-attatchment" {
  device_name = var.device-name
  volume_id   = aws_ebs_volume.ebs-volume.id
  instance_id = var.instance-id
}