#!/bin/bash


KEY='ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgg20LGSQ6DNTJL1L5DXZjYWZ8w5/9ifQG5A3DXraTPzDPcUW8m9wAf1vY7lkIIkq5mlqSRR31zKpAhtH9ave+MR+apGZl7CIz9f39WbUZs/XwPh5YPfyTfJPBlOhoSAejMStmQDHA7vFi/WUH9MaflcDf4m/jlkt4/zXGHpsbXBzeqtSJeI+rvpETVQ/lIsuW01ceQ4dyQ6o1Z+sLBRgbzPuUIPRoHIB/Q8/ea7ylZ8Js74kzF3nH/qcpJclMKzQXxDIOvYLT2n7z7jggIMURTdK1wUsNbmPmO069363EoiyET8MhUHLR2hDpNIHdTioUlhvrMszyRqdYR+W2X9Dz TerraFormKey'
PASS='$6$hbSJc366$j2/CuG.92csyUWUBgqW4EsrHl3VQJ39oPUpbdyGuLjOQxnVaLnF2XeeCPePubgJ/3KvHvyZ2VL1U/iAw.tHAn.'

useradd -m -g wheel -p $PASS devops 

mkdir /home/devops/.ssh -p
chown devops /home/devops/.ssh

echo $KEY > /home/devops/.ssh/authorized_keys
chmod 600 /home/devops/.ssh/authorized_keys

