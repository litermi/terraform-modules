module "ami" {
  source = "../AMI"
  ami-name = var.ami-name
  ami-owner = var.ami-owner
}
resource "aws_instance" "ec2-instance" {
  ami                         = module.ami.ami-id
  instance_type               = var.instance-type
  key_name                    = var.ssh-key
  subnet_id                   = var.subnet-id
  private_ip                 = var.private-ip
  vpc_security_group_ids             = var.security-groups
  availability_zone           = var.availability-zone
  associate_public_ip_address = var.add-public-ip
  monitoring                  = var.monitoring

  tags = {
    Name      = var.instance-name
    CreatedBy = "Terraform"
    Env       = var.env
  }

  ebs_block_device {
    device_name = "/dev/xvda"
    volume_size = var.volume-size
  }
  lifecycle {
    ignore_changes = [
      ebs_block_device,ami,ebs_optimized,user_data
    ]
  }

}

