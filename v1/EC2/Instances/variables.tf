variable "instance-name" {}

variable "env" {
    default = "Test"
}

variable "instance-type" {
  default     = "t2.micro"
}

variable "ssh-key" {
    default = "TerraFormKey"
}

variable "security-groups" {
    type = list(string)
}


variable "availability-zone" {
    default = null
}

variable "subnet-id" {
    default = null
}
variable "private-ip" {
    default = null

}

variable "add-public-ip" {
    default = false
}

variable "monitoring" {
    default = true
}


variable "ami-name" {
    type = list(string)
    default = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
}

variable "ami-owner" {
    type = list(string)
    default = ["137112412989"]
}

variable "volume-size" {
    default = 12
}