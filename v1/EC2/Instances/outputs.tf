# Define outputs
output "instance-id" {
  value = aws_instance.ec2-instance.id
}

output "instance-private-ip" {
  value = aws_instance.ec2-instance.private_ip
}
