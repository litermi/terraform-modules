data "aws_ami" "ami" {
  most_recent = true
  owners      = var.ami-owner


  filter {
    name   = "name"
    values = var.ami-name
  }
}
