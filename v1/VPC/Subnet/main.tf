resource "aws_subnet" "subnet" {
  vpc_id            = var.vpcId
  cidr_block        = var.cidr-block
  availability_zone = var.availability-zone
  map_public_ip_on_launch = var.mapPublicIp



  tags = {
    Name       = var.name
    CreatedBy  = "TerraForm"
    Env        = var.env

  }
  lifecycle {
    ignore_changes = [
     tags
    ]
  }

}
