variable "vpcId" {}
variable "cidr-block" {}
variable "availability-zone" {}
variable "name" {}
variable "mapPublicIp" {
    default = true
}
variable "env"{
    default = "Test"
}
