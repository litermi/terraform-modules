resource "aws_route" "routeTable" {
    vpc_id = ${var.vpc-id}
      route {
        cidr_block = var.cidr_block
        gateway_id = var.internet-gateway-id
      }
}