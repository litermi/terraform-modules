variable "internet-gateway-id" {}

variable "cidr_block" {
    default = "0.0.0.0/0"
}