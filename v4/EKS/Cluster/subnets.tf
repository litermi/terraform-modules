
resource "aws_subnet" "subnets" {

  for_each = tomap(var.SubnetList)
  availability_zone = each.key
  cidr_block = each.value
  vpc_id = var.VpcId
  map_public_ip_on_launch = var.MapPublicIpOnLaunch


  tags = {
    Name = "${var.ClusterName} LAN - ${each.key}"
  }
}
