variable "ClusterName" {}
variable "VpcId" {}
variable "MapPublicIpOnLaunch" {default=true}
variable "Version" {default=null}
variable "AuthenticationMode" {default="API_AND_CONFIG_MAP"}
variable "SubnetList" {
  type = map(any)
  /*default = {
    "ap-south-1a" = "10.0.1.0/24"
    "ap-south-1b" = "10.0.2.0/24"
  }*/
}
