output "endpoint" {
  value = aws_eks_cluster.Cluster.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.Cluster.certificate_authority[0].data
}

output "SubnetsIds"{
  value = [for subnet in aws_subnet.subnets : subnet.id]
}

output "ClusterName" {
  value = aws_eks_cluster.Cluster.name
}