
resource "aws_iam_role" "iamRole" {
  name = "eks-cluster-${var.ClusterName}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}



resource "aws_iam_policy" "CloudWatchPolicy" {
  name        = "Eks-${var.ClusterName}-CloudWatchPolicy"
  path        = "/"
  description = "Policy to allow write to CloudWatch"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "cloudwatch:PutMetricData",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}



resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.iamRole.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.iamRole.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSCloudWatchWrite" {
  policy_arn = aws_iam_policy.CloudWatchPolicy.arn
  role       = aws_iam_role.iamRole.name
}






