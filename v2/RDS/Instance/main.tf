resource "aws_subnet" "rds1" {
   vpc_id = var.VpcId
   cidr_block = var.Subnet1CidrBlock
   map_public_ip_on_launch = var.PublicIpOnLaunch
   availability_zone = var.Subnet1AvailabilityZone
   tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = "${var.Name} RDS LAN - 1"
    }
  )
 }


resource "aws_subnet" "rds2" {
   vpc_id = var.VpcId
   cidr_block = var.Subnet2CidrBlock
   map_public_ip_on_launch = var.PublicIpOnLaunch
   availability_zone = var.Subnet2AvailabilityZone
   tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = "${var.Name} RDS LAN - 2"
    }
  )
 }

resource "aws_db_subnet_group" "default" {
  name = format("%s-rds-subnet-group",lower(var.Name))
  description = var.Description
  subnet_ids  = [aws_subnet.rds1.id,aws_subnet.rds2.id]
}


resource "aws_db_instance" "rds-instance" {
  identifier = lower(var.Name)
  allocated_storage = var.AllocatedStorage
  engine = var.Engine
  engine_version = var.EngineVersion
  instance_class = var.InstanceClass
  db_name = var.SnapshotIdentifier != "" ? null : lower(var.Name)
  username = var.UserName
  password = var.Password
  apply_immediately = var.ApplyImmediately
  parameter_group_name = var.ParameterGroupName
  publicly_accessible = var.PubliclyAccessible
  db_subnet_group_name = aws_db_subnet_group.default.id
  vpc_security_group_ids = var.SecurityGroups
  skip_final_snapshot = var.SkipFinalSnapshot
  final_snapshot_identifier = var.FinalSnapshotIdentifier
  backup_retention_period = var.BackupRetentionPeriod
  snapshot_identifier = var.SnapshotIdentifier
}

