variable "VpcId" {}
variable "Name" {default=null}
variable "Description" {default=null}

variable "Subnet1CidrBlock" {}
variable "Subnet1AvailabilityZone" {}
variable "Subnet2CidrBlock" {}
variable "Subnet2AvailabilityZone" {}


variable "ParameterGroupName" {default=null}
variable "AllocatedStorage" {default=16}
variable "Engine" {default=null}
variable "SnapshotIdentifier" {default=null}
variable "EngineVersion" {default=null}
variable "InstanceClass" {default="db.t2.micro"}
variable "UserName" {default="master"}
variable "Password" {default=null}
variable "BackupRetentionPeriod" {default=1}
variable "FinalSnapshotIdentifier" {default="Ignore"}
variable "ApplyImmediately" {default=false}
variable "PublicIpOnLaunch" {
    type = bool
    default = true
}
variable "SkipFinalSnapshot" {
    type = bool 
    default = true
}
variable "PubliclyAccessible" {
    type = bool 
    default = true
}

variable "SecurityGroups" {
    type = list(string)
}

variable "Tags" {
    type = map
    default = null
}
