variable "Name" {}
variable "Family" {default="mysql8.0"}
variable "Parameters" {
  type = list(map(string))
}
