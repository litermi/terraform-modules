resource "aws_db_parameter_group" "group" {
  name   = var.Name
  family = var.Family

  dynamic "parameter" {
    for_each = var.Parameters
    content {
      name  = parameter.value.Name
      value = parameter.value.Value
      apply_method = "pending-reboot"
    }
  }

  
  lifecycle {
    create_before_destroy = true
  }
}
