variable "domainName" {
    default = null
}
variable "originId" {}
variable "originDomainName" {}
variable "aliases" {
    default = null
}

variable "DefaultCertificate" { 
  default = true 
}
variable "priceClass" {
    default = "PriceClass_100"
}

variable "allowedMethods" {
    default = ["GET"]
    type = list(string)
}

variable "cachedMethods" {
    default = ["GET"]
    type = list(string)
}

variable "protocolPolicy" {
    default = "redirect-to-https"
}

variable "restrictionLocations" {
    default = null
}

variable "restrictionType" {
    default = "none"
}

variable "sslCert" {
    default = null
}

variable "Tags" { 
    type = map
    default = null
}

variable "Environment" {
    default = "Test"
}
