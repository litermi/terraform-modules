

resource "aws_cloudfront_distribution" "site_distribution" {
  origin {
    domain_name = var.originDomainName
    origin_id = var.originId
  }
  enabled = true
  aliases = var.aliases
  price_class = var.priceClass
  default_root_object = "index.html"

  restrictions {
    geo_restriction {
      locations = var.restrictionLocations
      restriction_type = var.restrictionType
    }
  }

  default_cache_behavior {
    viewer_protocol_policy = "redirect-to-https"
    compress = true
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = var.originId

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Environment" = var.Environment
    }
  )
  viewer_certificate {
    cloudfront_default_certificate = var.DefaultCertificate
  }
}
