variable "Name" {}
variable "Environment" {
    default = "Test"
}
variable "Acl" {
    default = "private"
}
variable "Versioning" {
    default = false
}
variable "PreventDestroy"{
    default = false
}


variable "Tags" { 
    type = map
    default = null
}
