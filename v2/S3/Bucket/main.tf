resource "aws_s3_bucket" "s3-bucket" {
  bucket = var.Name
  acl    = var.Acl

  versioning {
    # enable with caution, makes deleting S3 buckets tricky
    enabled = var.Versioning
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
      "Environment" = var.Environment
    }
  )
}
