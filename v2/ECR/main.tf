resource "aws_ecr_repository" "repository" {
  name = var.Name
  image_tag_mutability = var.Mutable

  image_scanning_configuration {
    scan_on_push = var.ScanOnPush
  }
}