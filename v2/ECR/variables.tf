variable "Name" {}
variable "Mutable" {
    default = "MUTABLE"
}
variable "ScanOnPush" {
    default = true
}