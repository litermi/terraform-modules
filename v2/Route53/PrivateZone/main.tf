resource "aws_route53_zone" "private" {
  name = var.Name

  vpc {
    vpc_id = var.VpcId
  }

}
