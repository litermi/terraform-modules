resource "aws_route53_record" "record" {
  zone_id = var.ZoneId
  name    = var.RecordName
  type    = var.RecordType
  ttl     = var.TTL
  records = var.RecordValues
}
