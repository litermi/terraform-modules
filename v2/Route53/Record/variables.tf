variable "ZoneId" {}
variable "RecordName" {}
variable "RecordType" {default="A"}
variable "TTL" {default=300}
variable "RecordValues" {
  type = list(string)
}

