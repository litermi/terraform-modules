resource "aws_vpc" "vpc" {
  cidr_block = var.CidrBlock

  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.VpcName
    }
  )
}
