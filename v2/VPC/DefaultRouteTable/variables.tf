variable "DefaultRouteTableId" {}
variable "Name" {}
variable "CidrBlock" {default="0.0.0.0/0"}
variable "InternetGatewayId" {}
variable "Tags" { 
    type = map
    default = null
}