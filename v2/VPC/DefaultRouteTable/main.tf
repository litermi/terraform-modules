resource "aws_default_route_table" "default-route-table" {
  default_route_table_id = var.DefaultRouteTableId

  route {
    cidr_block = var.CidrBlock
    gateway_id = var.InternetGatewayId
  }

  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )
}