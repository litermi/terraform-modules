resource "aws_route_table_association" "a" {
  subnet_id = var.SubnetId
  route_table_id = var.RouteTableId
}