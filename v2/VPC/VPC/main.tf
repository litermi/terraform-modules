resource "aws_vpc" "vpc" {
  cidr_block = var.CidrBlock
  enable_dns_hostnames = var.enableDnsHostnames
  enable_dns_support = var.enableDnsSupport
  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )
}

resource "aws_internet_gateway" "internet-gateway" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_default_route_table" "default-route-table" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gateway.id
  }
  route {
    ipv6_cidr_block = "::/0"
    gateway_id = aws_internet_gateway.internet-gateway.id
  }

  tags = {
    "CreatedBy" = "Terraform"
    "Name" = "Internet Gateway - ${var.Name}"
  }
  
}
