variable "Name"{}
variable "CidrBlock" {}
variable "Tags" {type = map}
variable "enableDnsSupport" {
   type = bool
   default = true
}

variable "enableDnsHostnames" {
   type = bool
   default = true
}
