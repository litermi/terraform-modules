variable "InternetGatewayId" {}
variable "VpcId" {}
variable "CidrBlock" {default="0.0.0.0/0"}
variable "Name" {}
variable "Tags" { 
    type = map
    default = null
}
