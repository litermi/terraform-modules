resource "aws_route_table" "routeTable" {
  vpc_id = var.VpcId
  route {
    cidr_block = var.CidrBlock
    gateway_id = var.InternetGatewayId
  }
  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )
}