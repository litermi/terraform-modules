resource "aws_subnet" "subnet" {
  vpc_id            = var.VpcId
  cidr_block        = var.CidrBlock
  availability_zone = var.AvailabilityZone
  map_public_ip_on_launch = var.MapPublicIpOnLaunch
  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )
}
