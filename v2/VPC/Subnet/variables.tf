variable "VpcId" {}
variable "CidrBlock" {}
variable "AvailabilityZone" {}
variable "Name" {}
variable "MapPublicIpOnLaunch" {default = true}
variable "Tags" { 
    type = map
    default = null
}