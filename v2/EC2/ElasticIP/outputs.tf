# Define Public IP.
output "Ip" {
  value = aws_eip.elastic-ip.public_ip
}

output "Id" {
  value = aws_eip.elastic-ip.id
}