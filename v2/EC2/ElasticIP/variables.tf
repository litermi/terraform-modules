variable "InstanceId" {default=null}
variable "Name" {}
variable "IsInVpc" {default=true}
variable "Tags" { 
    type = map
    default = null
}