# Add New Elastic IP To instance
resource "aws_eip" "elastic-ip" {
  instance = var.InstanceId
  vpc = var.IsInVpc
  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )
}



