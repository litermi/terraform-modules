resource "aws_ebs_volume" "ebs-volume" {
  availability_zone = var.AvailabilityZone
  size = var.Size
  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )
}

resource "aws_volume_attachment" "ebs-volume-attatchment" {
  device_name = var.DeviceName
  volume_id   = aws_ebs_volume.ebs-volume.id
  instance_id = var.InstanceId
}