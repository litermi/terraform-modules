variable "Name" {}
variable "AvailabilityZone" {}
variable "Size" {default=12}
variable "DeviceName" {}
variable "InstanceId" {}
variable "Tags" { 
    type = map
    default = null
}