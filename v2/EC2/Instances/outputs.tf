# Define outputs
output "Id" {
  value = aws_instance.ec2-instance.id
}

output "PrivateIp" {
  value = aws_instance.ec2-instance.private_ip
}

output "AvailabilityZone" {
  value = aws_instance.ec2-instance.availability_zone
}