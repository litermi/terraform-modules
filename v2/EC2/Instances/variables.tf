variable "Name" {}
variable "InstanceType" {default="t2.micro"}
variable "SshKey" {default = "TerraFormKey"}
variable "SecurityGroups" {type = list(string)}
variable "AvailabilityZone" {default = null}
variable "SubnetId" {default = null}
variable "PrivateIp" {default = null}
variable "AddPublicIp" {default = true}
variable "Monitoring" {default = true}
variable "VolumeSize" {default = 20}
variable "RecreateWhenUserDataChanges" {default = false}
variable "RemoteUser" {default = "ec2-user"}
variable "ProvisionAnsible" {default = false}
variable "TerraformKey" {default = "TERRAFORM_KEY_AWS_DE"}
variable "ProvisionAnsibleCustom" {
    type = list(string)
    default = []
}
variable "ProvisionAnsibleBaseFile" {
    type = string
    default = "BaseAmazonLinux.yml"
}
variable "UserData" {
    default = null
    type = string
}
variable "AmiName" {
    type = list(string)
    default = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
}
variable "AmiOwner" {
    type = list(string)
    default = ["137112412989"]
}
variable "Tags" { 
    type = map
    default = null
}
