data "aws_ami" "ami" {
  most_recent = true
  owners      = var.AmiOwner
  filter {
    name   = "name"
    values = var.AmiName
  }
}

resource "aws_instance" "ec2-instance" {
  ami = data.aws_ami.ami.id
  instance_type = var.InstanceType
  key_name = var.SshKey
  subnet_id = var.SubnetId
  private_ip = var.PrivateIp
  vpc_security_group_ids = var.SecurityGroups
  availability_zone = var.AvailabilityZone
  associate_public_ip_address = var.AddPublicIp
  monitoring = var.Monitoring
  user_data = var.UserData
  user_data_replace_on_change = var.RecreateWhenUserDataChanges

  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )

  ebs_block_device {
    device_name = "/dev/xvda"
    volume_size = var.VolumeSize
  }
  lifecycle {
    ignore_changes = [
      ebs_block_device,ami,ebs_optimized
    ]
  }

  provisioner "local-exec" {
    command = (
        format(
          "%s %s", 
              var.ProvisionAnsible ? <<-EOT
                export ANSIBLE_HOST_KEY_CHECKING=False
                echo ${"$"}${var.TerraformKey} | base64 -d > /tmp/id_rsa 
                cat /tmp/id_rsa
                chown 600 /tmp/id_rsa
                cd ${path.module}/ansible
                sleep 120
                ansible-playbook -u ${var.RemoteUser} -i '${self.public_ip},'  --private-key /tmp/id_rsa ${var.ProvisionAnsibleBaseFile} 

                EOT
              : 
              "",
              var.ProvisionAnsible ? <<-EOT
                   %{ for file in var.ProvisionAnsibleCustom ~}
                     ansible-playbook -u ${var.RemoteUser} -i '${self.public_ip},' --private-key /tmp/id_rsa ${file} 

                   %{ endfor }
              EOT
              :
              ""
        )
    )
  }

}

