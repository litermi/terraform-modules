variable "Name" {}
variable "AvailabilityZone" {}
variable "Size" {default=12}
variable "Tags" { 
    type = map
    default = null
}
