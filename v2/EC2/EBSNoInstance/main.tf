resource "aws_ebs_volume" "ebs-volume" {
  availability_zone = var.AvailabilityZone
  size = var.Size
  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = var.Name
    }
  )
}

