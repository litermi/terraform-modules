#!/bin/bash
kubectl apply -f $INGRESS_PATH/Base.yml
if [ "$CREATE_PUBLIC_INGRESS" = "true" ]
then
    kubectl apply -f $INGRESS_PATH/PublicLoadBalancer.yml
fi 

if [ "$CREATE_PRIVATE_INGRESS" = "true" ]
then
    kubectl apply -f $INGRESS_PATH/PrivateLoadBalancer.yml
fi 

if [ "$CREATE_INTERNAL_INGRESS" = "true" ]
then
    kubectl apply -f $INGRESS_PATH/InternalLoadBalancer.yml
fi 