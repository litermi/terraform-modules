 module "EksCluster" {
  source = "bitbucket.org/litermi/terraform-modules//v2/EKS/Cluster"
  SubnetList = var.SubnetList
  ClusterName = var.ClusterName
  VpcId = var.VpcId
  Version = var.Version
}

module "NodeGroups" {
  for_each = tomap(var.NodeGroups)
  source = "bitbucket.org/litermi/terraform-modules//v2/EKS/NodeGroup"
  ClusterName = module.EksCluster.ClusterName
  SubnetsIds = module.EksCluster.SubnetsIds
  GroupName = each.key
  DiskSize = try(each.value.DiskSize,null)
  CapacityType = try(each.value.CapacityType, "ON_DEMAND")
  InstanceType = try(each.value.InstanceType,null)
  DesiredSize = try(each.value.DesiredSize,3)
  MinSize = try(each.value.MinSize,2)
  MaxSize = try(each.value.MaxSize,4)
  MaxUnavailable = try(each.value.MaxUnavailable, 1)
  Labels = try(each.value.Labels,null)
  Tags = try(each.value.Tags,null)
}

module "ElasticIps" {
  for_each = tomap(var.SubnetList)
  source = "bitbucket.org/litermi/terraform-modules//v2/EC2/ElasticIP/"
  Name = "EKS - ${var.ClusterName} - ${each.key}"
}

resource "aws_security_group" "sec-group" {
  count = var.CreatePrivateIngress == "true" ? 1 : 0
  name   = "${var.ClusterName} - Internal LB"
  vpc_id = var.VpcId

  ingress {
    from_port   = "443"
    to_port     = "443"
    protocol    = "tcp"
    cidr_blocks = var.PrivateIngressRanges
  }

  ingress {
    from_port   = "80"
    to_port     = "80"
    protocol    = "tcp"
    cidr_blocks = var.PrivateIngressRanges
  }
    egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name       = "${var.ClusterName} - Internal LB"
    CreatedBy  = "TerraForm"
    Env        = "Test"
  }
}


resource "null_resource" "provisioning" {
  depends_on = [
    module.NodeGroups
  ]
  provisioner "local-exec" {
    command = <<EOF

      export CLUSTER_NAME=${var.ClusterName}
      export CREATE_PUBLIC_INGRESS=${var.CreatePublicIngress}
      export CREATE_PRIVATE_INGRESS=${var.CreatePrivateIngress}
      export CREATE_INTERNAL_INGRESS=${var.CreateInternalIngress}
      export CREATE_CLOUDWATCH_AGENT=${var.CreateCloudWatchAgent}
      export KUBERNETES_VERSION=${try(var.Version,"Latest")}
      export INGRESS_PATH="IngressNginx"

      cd ${path.module}/Provisioning


      if [ "$KUBERNETES_VERSION" = "1.21" ]
      then
        export INGRESS_PATH="IngressNginx-K8s1.21"
      fi

      if [ "${var.CreateCloudWatchAgent}" = "true" ]
      then
        sed -i 's/__CLUSTER_NAME__/${var.ClusterName}/g' CloudWatchAgent/03-ConfigMap-CloudWatchAgent.yml
        sed -i 's/__CLUSTER_NAME__/${var.ClusterName}/g' CloudWatchAgent/03-ConfigMap-FluentBit.yml
      fi

      if [ "${var.CreatePrivateIngress}" = "true" ]
      then 
        sed -i 's/__SEC_GROUP_ID__/${try(aws_security_group.sec-group[0].id," ")}/g' $INGRESS_PATH/PrivateLoadBalancer.yml
        sed -i 's/__ELASTIC_IPS__/${join(",",[for eip in module.ElasticIps : eip.Id])}/g' $INGRESS_PATH/PrivateLoadBalancer.yml
        sed -i 's/__NETWORK_RANGES__/${replace(join(",",var.PrivateIngressRanges),"/","___")}/g' $INGRESS_PATH/PrivateLoadBalancer.yml
        sed -i 's/___/\//g' $INGRESS_PATH/PrivateLoadBalancer.yml

      fi

      source Scripts/Init.sh
      source Scripts/Ingress.sh
      source Scripts/LetsEncrypt.sh
      source Scripts/CloudWatchAgent.sh


    EOF
  }
}
