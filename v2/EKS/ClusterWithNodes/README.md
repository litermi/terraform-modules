# EKS Cluster creation with Node Groups
This Terraform module creates an EKS cluster, with NodeGroups, and initialize it with the Nginx Ingress Controller and Let's Encrypt Certificate Issuer.

## Field reference

- `ClusterName` (string): The name for the cluster
- `VpcId` (string): The VPC ID where the cluster will be created
- `MapPublicIPOnLaunch` (bool): Specify if the subnets will add public IPs to the instances that will be created on them. Defaults to true.
- `Version`(string): The Kubernetes version to be used. Defaults to the latest version
- `SubnetList` (map): The subnets to be created for the cluster. Must specify a map with the subnets name and CIDR
- `NodeGroups` (map): The characteristics for the Node groups to be created. In here you specify the group name, autoscaling settings, disk size, among others. These are all the parameters for the Node Groups:
	- `GroupName` (string): Name for the Node Group
	- `DiskSize` (integer): Disk size for the instances on the Node Group. Defaults to 20GB
	- `CapacityType` (string): Specify if the NodeGroup will use `ON_DEMAND` (default) or `SPOT` instances.
	- `InstanceType` (string): The instance type for the instances on the group. Defaults to `t3.medium`
	- `DesiredSize` (integer): the desired Instance count for autoscaling. Defaults to 3
	- `MinSize` (integer): the minimal Instance count for autoscaling. Defaults to 2
	- `MaxSize` (integer): the maximum Instance count for autoscaling. Defaults to 4.
	- `MaxUnavailable` (integer): The maximum Instance down count tolerated. Defaults to 1.
	- `Tags` (map): Tags to be added for the Node Group
	- `Labels` (map): Kubernetes Labels to be added to the Node Group


##  Example

```terraform
module "EksCluster" {

	source = "bitbucket.org/litermi/terraform-modules//v2/EKS/ClusterWithNodes"
	SubnetList = {
		"eu-central-1a" = "10.0.11.0/24"
		"eu-central-1b" = "10.0.12.0/24"
		"eu-central-1c" = "10.0.13.0/24"
	}
	ClusterName = "EksCluster"
	VpcId = "vpc-XXXXX"
	NodeGroups = {
		"Production" = {
			"InstanceType" = "c5.xlarge"
			"DiskSize" = 120
			"CapacityType" = "ON_DEMAND"
			"DesiredSize" = 6
			"MinSize" = 4
			"MaxSize" = 10
			"MaxUnavailable" = 2
			"Labels" = {
				"Environment" = "Production"
				"InstanceType" = "OnDemand"
			}
			"Tags" = {
				"Environment" = "Production"
				"CreatedBy" = "Terraform"
			}
		}
		"Test" = {
			"InstanceType" = "t3.small"
			"DesiredSize" = 3
			"MinSize" = 2
			"MaxSize" = 4
			"MaxUnavailable" = 1
			"Labels" = {
				"Environment" = "Test"
				"InstanceType" = "OnDemand"
			}
		}
	}
}
```