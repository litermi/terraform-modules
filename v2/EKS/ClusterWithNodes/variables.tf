variable "ClusterName" {}
variable "VpcId" {}
variable "CreatePublicIngress" {default="true"}
variable "CreatePrivateIngress" {default="false"}
variable "CreateInternalIngress" {default="false"}
variable "CreateCloudWatchAgent" {default="false"}
variable "PrivateIngressRanges" {
  type = list(string)
  default = null
}
variable "MapPublicIpOnLaunch" {default=true}
variable "Version" {default=null}
variable "SubnetList" {
  type = map(any)
}
variable "NodeGroups" {
  type = map(any)
}

