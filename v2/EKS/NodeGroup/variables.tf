variable "ClusterName" {}
variable "GroupName" {}
variable "DiskSize" {default=null}
variable "CapacityType" {default=null}
variable "InstanceType"{default="t3.medium"}
variable "DesiredSize" {default=3}
variable "MaxSize" {default=4}
variable "MinSize" {default=2}
variable "MaxUnavailable" {default=1}
variable "Tags" { 
    type = map
    default = null
}

variable "Labels" {
    type = map(any)
    default = null
}
variable "SubnetsIds" {
    type = list(string)
}