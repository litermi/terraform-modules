resource "aws_iam_user" "user" {
  name = var.Name
  path = var.Path

  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
    }
  )
}