output "Id" {
  value = aws_iam_user.user.unique_id
}

output "Arn" {
  value = aws_iam_user.user.arn
}

output "Name" {
  value = aws_iam_user.user.name
}

