output "Id" {
  value = aws_iam_group.group.id
}

output "Arn" {
  value = aws_iam_group.group.arn
}

output "Name" {
  value = aws_iam_group.group.name
}

output "UniqueId" {
  value = aws_iam_group.group.unique_id
}


