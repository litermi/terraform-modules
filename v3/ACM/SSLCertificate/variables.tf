
variable "Domain" {}
variable "AlternativeNames"{
    default = null
    type = list(string)
}
variable "Env" {
    default = "Test"
}

variable "Tags" { 
    type = map
    default = null
}
