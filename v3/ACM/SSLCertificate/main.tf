
resource "aws_acm_certificate" "cert" {
  domain_name       = var.Domain
  subject_alternative_names = var.AlternativeNames

  validation_method = "DNS"

  tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = trim(var.Domain,"*")

    }
  )



  lifecycle {
    create_before_destroy = true
  }
}
