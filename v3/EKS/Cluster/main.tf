resource "aws_eks_cluster" "Cluster" {
  name = var.ClusterName
  role_arn = aws_iam_role.iamRole.arn

  vpc_config {
    subnet_ids = [for subnet in aws_subnet.subnets : subnet.id]
  }
  version = var.Version
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
  ]
}
