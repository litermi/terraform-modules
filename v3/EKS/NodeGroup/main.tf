resource "aws_eks_node_group" "nodeGroup" {
  cluster_name = var.ClusterName
  node_group_name = var.GroupName
  node_role_arn = aws_iam_role.iamRole.arn
  subnet_ids = var.SubnetsIds
  disk_size = var.DiskSize
  capacity_type = var.CapacityType
  instance_types = [var.InstanceType]
  scaling_config {
    desired_size = var.DesiredSize
    max_size = var.MaxSize
    min_size = var.MinSize
  }

   tags = merge(
    var.Tags, {
      "CreatedBy" = "Terraform"
      "Name" = "${var.ClusterName} - ${var.GroupName}"
    }
  )

  labels = var.Labels
  update_config {
    max_unavailable = var.MaxUnavailable
  }

  lifecycle {
    ignore_changes = [scaling_config[0].desired_size]
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]
}