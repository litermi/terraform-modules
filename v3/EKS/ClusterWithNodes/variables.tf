variable "ClusterName" {}
variable "VpcId" {}
variable "CreatePublicIngress" {default="true"}
variable "CreateInternalIngress" {default="false"}
variable "CreateCloudWatchAgent" {default="false"}
variable "Region" {default="eu-central-1"}
variable "MapPublicIpOnLaunch" {default=true}
variable "Version" {default=null}
variable "SubnetList" {
  type = map(any)
}
variable "NodeGroups" {
  type = map(any)
}

