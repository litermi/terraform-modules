 module "EksCluster" {
  source = "bitbucket.org/litermi/terraform-modules//v3/EKS/Cluster"
  SubnetList = var.SubnetList
  ClusterName = var.ClusterName
  VpcId = var.VpcId
  Version = var.Version
}

module "NodeGroups" {
  for_each = tomap(var.NodeGroups)
  source = "bitbucket.org/litermi/terraform-modules//v3/EKS/NodeGroup"
  ClusterName = module.EksCluster.ClusterName
  SubnetsIds = module.EksCluster.SubnetsIds
  GroupName = each.key
  DiskSize = try(each.value.DiskSize,null)
  CapacityType = try(each.value.CapacityType, "ON_DEMAND")
  InstanceType = try(each.value.InstanceType,null)
  DesiredSize = try(each.value.DesiredSize,3)
  MinSize = try(each.value.MinSize,2)
  MaxSize = try(each.value.MaxSize,4)
  MaxUnavailable = try(each.value.MaxUnavailable, 1)
  Labels = try(each.value.Labels,null)
  Tags = try(each.value.Tags,null)
}

module "ElasticIps" {
  for_each = tomap(var.SubnetList)
  source = "bitbucket.org/litermi/terraform-modules//v2/EC2/ElasticIP/"
  Name = "EKS - ${var.ClusterName} - ${each.key}"
}


resource "null_resource" "provisioning" {
  depends_on = [
    module.NodeGroups
  ]
  provisioner "local-exec" {
    command = <<EOF

      export CLUSTER_NAME=${var.ClusterName}
      export CREATE_PUBLIC_INGRESS=${var.CreatePublicIngress}
      export CREATE_INTERNAL_INGRESS=${var.CreateInternalIngress}
      export CREATE_CLOUDWATCH_AGENT=${var.CreateCloudWatchAgent}
      export KUBERNETES_VERSION=${try(var.Version,"Latest")}
      export INGRESS_PATH="IngressNginx"

      cd ${path.module}/Provisioning

      if [ "${var.CreateCloudWatchAgent}" = "true" ]
      then
        sed -i 's/__CLUSTER_NAME__/${var.ClusterName}/g' CloudWatchAgent/03-ConfigMap-CloudWatchAgent.yml
        sed -i 's/__CLUSTER_NAME__/${var.ClusterName}/g' CloudWatchAgent/03-ConfigMap-FluentBit.yml
        sed -i 's/__REGION__/${var.Region}/g' CloudWatchAgent/03-ConfigMap-FluentBit.yml

      fi

      sed -i 's/__ELASTIC_IPS__/${join(",",[for eip in module.ElasticIps : eip.Id])}/g' $INGRESS_PATH/PublicLoadBalancer.yml

      

      source Scripts/Init.sh
      source Scripts/Ingress.sh
      source Scripts/LetsEncrypt.sh

      if [ "${var.CreateCloudWatchAgent}" = "true" ]
      then
        source Scripts/CloudWatchAgent.sh
      fi


    EOF
  }
}
