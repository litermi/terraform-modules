variable "name" {}
variable "region" {}
variable "acl" {
    default = "private"
}
variable "versioning" {
    default = false
}
variable "prevent-destroy"{
    default = false
}

